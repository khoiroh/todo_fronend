import React from "react";
import { useHistory } from "react-router-dom";

export default function () {
  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/");
  };

  return (
    <div>
      <hr className="bg-green-800 h-1" />
      <div className="flex bg-green-900 text-gray-200 font-mono p-4">
        <div className="flex gap-3 text-center">
          <a href="/">
            <img className="hover:origin-top-left hover:rotate-12"
              src="https://cdn-icons-png.flaticon.com/512/7595/7595571.png"
              alt=""
              style={{ width: 50 }}
            />
          </a>
          <h1 className="mt-2 text-2xl">
            <b>Attendance & todo-list</b>
          </h1>
        </div>
        {localStorage.getItem("id") !== null ? (
          <>
            <div className="ml-auto">
              <a className="btn btn-success" href="/profil">
                <i class="fas fa-user-alt"></i>{" "}
              </a>
            </div>
            <div className="ml-2">
              <a className="btn btn-success" onClick={logout}>
                <i class="fas fa-sign-out-alt"></i>
              </a>
            </div>
          </>
        ) : (
          <div className="ml-auto">
            <a className="btn btn-success" href="/login">
              <i class="fas fa-sign-in-alt"></i>
            </a>
          </div>
        )}
      </div>
      <hr className="h-2 bg-green-900" />
    </div>
  );
}
