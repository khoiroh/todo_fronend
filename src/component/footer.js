import React from "react";

export default function () {
  return (
    <div>
      <footer class="p-4 bg-white rounded-lg md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800">
        <span class="text-sm text-gray-500 sm:text-center dark:text-gray-400">
          © 2023{" "}
          <a href="" class="hover:underline">
          Attendance & todo-list
          </a>
          . All Rights Reserved.
        </span>
        <ul class="flex flex-wrap items-center mt-3 text-sm text-gray-500 dark:text-gray-400 sm:mt-0">
          <li>
            <a href="/" class="mr-4 hover:underline md:mr-6 ">
              Home
            </a>
          </li>
          <li>
            <a href="/todo" class="mr-4 hover:underline md:mr-6">
              Todo-List
            </a>
          </li>
          <li>
            <a href="/absen" class="mr-4 hover:underline md:mr-6">
              Presensi
            </a>
          </li>
          <li>
            <a href="/profil" class="mr-4 hover:underline md:mr-6">
              Profile
            </a>
          </li>
          
        </ul>
      </footer>
    </div>
  );
}
