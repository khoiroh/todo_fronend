import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Register from "./pages/Register";
import Loginn from "./pages/Loginn";
import TodoList from "./pages/TodoList";
import Home from "./pages/Home";
import Navbar from "./component/Navbar";
import Presensi from "./pages/Presensi";
import Footer from "./component/footer";
import Update from "./pages/Update";
import Profile from "./pages/Profile";
import Prof from "./pages/Prof";

const App = () => {
  return (
    <div className="">
      <div className="app-wrapper">
        <BrowserRouter>
          <main>
            <Switch>
                <Route path="/login" component={Loginn} exact />
                <Route path="/register" component={Register} exact />
                <Route path="/ediT/:id" component={Update} exact />
                <Route path="/profil" component={Profile} exact />
                <Route path="/pil" component={Prof} exact />
              <div>
                <Navbar />
                <Route path="/" component={Home} exact />
                <Route path="/todo" component={TodoList} exact />
                <Route path="/absen" component={Presensi} exact />
              </div>
            </Switch>
          </main>
        </BrowserRouter>
      </div>
    </div>
  );
};

export default App;
