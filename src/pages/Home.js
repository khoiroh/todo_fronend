import React from "react";
import { useHistory } from "react-router-dom";
import Footer from "../component/footer";
import image from "../image/undraw_selection_re_ycpo.png";

export default function () {
  return (
    <div>
      <div className="">
        <div className="block md:flex lg:flex">
          <div className=" mt-28">
            <div className="text-center">
              <h1 className="font-serif text-center text-4xl ml-18 lg:ml-32 text-green-900 font-semibold">
                Welcome ...!!!
              </h1>
            </div>
            <br />
            <div className="p-2">
              <p className="text-xl border-2 border-dashed border-green-900 rounded text-center ml-18 md:ml-20 lg:ml-32 text-green-600">
                <b>Login to the website to fill</b>
              </p>
            </div>

            {localStorage.getItem("id") == null ? (
              <></>
            ) : (
              <div className="flex mt-5 gap-5 ml-14 md:ml-24 lg:ml-32">
                <div class="w-40 md:w-44 drop-shadow-lg max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                  <h5 class="mb-2 text-2xl font-bold tracking-tight text-green-900 dark:text-white">
                    Absensi
                  </h5>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                    Silahkan Absensi
                  </p>
                  <a
                    href="/absen"
                    class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-700 rounded-lg hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                  >
                    <i class="fas fa-book"></i>
                  </a>
                </div>

                <div class="drop-shadow-lg max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 w-40 md:w-44">
                  <h5 class="mb-2 text-2xl font-bold tracking-tight text-green-900 dark:text-white">
                    Todo-List
                  </h5>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                    Silahkan masuk ...
                  </p>
                  <a
                    href="/todo"
                    class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-700 rounded-lg hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                  >
                    <i class="fas fa-list"></i>
                  </a>
                </div>
              </div>
            )}
          </div>
          <img
            className="ml-18 md:ml-auto  w-full md:w-1/2 mt-10"
            src={image}
            alt=""
          />
        </div>
      </div>
      <hr className="bg-green-800 h-2" />
      <Footer />
    </div>
  );
}
