import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

export default function () {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:3009/login1/sign-in",
        {
          email: email,
          password: password,
        }
      );
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login berhasil!!!",
          showCancelButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.user.id);
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showCancelButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  const history = useHistory();
  return (
    <div style={{}}>
      <div
        style={{ padding: 15, borderRadius: 10 }}
        className="container mt-10 w-full md:w-1/3 "
      >
        <form
          onSubmit={login}
          method="POST"
          class="backdrop-opacity-20 backdrop-invert bg-white/0 shadow-md rounded px-8 pt-6 pb-8 mb-4"
        >
          <div className="text-green-700 font-semibold">
            <h1 style={{ textAlign: "center", fontSize: 25 }}>Formulir Masuk</h1>
            {/* <h4 >
              <a className="buat" href="/register"><i class="fas fa-user-edit"></i></a> 
            </h4> */}
          </div>
          <hr style={{ height: 3, backgroundColor: "skyblue" }} />
          <div class="mb-4 mt-2">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="email"
            >
              Email
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div class="mb-4">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="password"
            >
              Password
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              placeholder="******************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-green-600 hover:bg-green-900 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Masuk
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-green-600 hover:text-green-800"
              href="/register"
            >
              Belum punya akun? Daftar
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}
