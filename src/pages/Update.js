import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function 
() {
    const param = useParams();
    const [taks, setTaks] = useState("");

    const history = useHistory();

    const updateT = async (e) => {
      e.preventDefault();
  
      // const fromData = new FormData();
      // fromData.append("nama", nama);
      // fromData.append("status", status);
  
      Swal.fire({
        title: "Yakin Ingin mengubah data ini?",
        text: "Anda tidak akan dapat mengembalikan data ini!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, edit it!",
      })
        .then((result) => {
          if (result.isConfirmed) {
            axios.put("http://localhost:3009/list/" + param.id, {
              taks: taks,
              userId: localStorage.getItem("userId"),
            });
          }
        })
        .then(() => {
          history.push("/todo");
          Swal.fire("Berhasil!!", "Data kamu berhasil di update,", "success");
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        })
        .catch((error) => {
          alert("Terjadinya kesalahan: " + error);
        });
    };

    useEffect(() => {
        axios
          .get("http://localhost:3009/list/" + param.id)
          .then((response) => {
            const newMenu = response.data.data;
            setTaks(newMenu.taks);
          })
          .catch((error) => {
            alert("Terjadi kesalahan Lurr!!! " + error);
          });
      }, []);
  return (
    <div>
         <div className="w-11/12 md:w-1/2 ">
        <div className="container ">
          <div className="">
            <Form onSubmit={updateT}>
              <h2 className="text-center text-2xl">Update Data :</h2>
              <div >
              <hr />
              <div class="mb-4 text-center">
                <label class="
                 text-gray-700 text-sm font-bold mb-2">
                  Taks
                </label>
                <input
                  class="text-center backdrop-opacity-20 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="taks"
                  placeholder="Taks"
                  value={taks}
                  onChange={(e) => setTaks(e.target.value)}
                />
              </div>
              </div>

              <div className="d-flex justify-content-end align-items-center mt-2">
                <button className="btn btn-primary w-full">
                  Save
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  )
}
