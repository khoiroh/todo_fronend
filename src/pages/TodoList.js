import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import Swal from "sweetalert2";
import image from "../image/undraw_To_do_list_re_9nt7.png";

export default function () {
  const [todo, setTodo] = useState([]);
  const [taks, setTeks] = useState("");

  const listt = async (e) => {
    e.preventDefault();
    try {
      const result = await axios.post(`http://localhost:3009/list/`, {
        userId: localStorage.getItem("id"),
        taks: taks,
      });
      console.log(result.data.data);
      // console.log(data);
      Swal.fire({
        icon: "success",
        title: "Berhasil !!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async () => {
    await axios
      .get(
        `http://localhost:3009/list/all?userId=${localStorage.getItem("id")}`,
        sessionStorage.setItem("CHECK", "P")
      )
      .then((res) => {
        setTodo(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const deleteUser = async (id) => {
    await Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3009/list/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
    getAll(0);
  };

  const validasi = async (id) => {
    axios.put("http://localhost:3009/list/selesai/" + id);
    Swal.fire("", "Selesai Mengerjakan", "success");
  };

  return (
    <div>
      <div className="block md:flex lg:flex">
        <img className="md:w-2/4 h-full " src={image} alt="" />
        <div className="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans lg:mt-20">
          <div className="bg-white rounded shadow-lg p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">
            <div className="mb-4 text-center">
              <h1 className="text-3xl font-bold text-green-800">Todo-List</h1>
            </div>

            <div className="mb-3 ">
              <Form onSubmit={listt}>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="Enter a Todo..."
                    value={taks}
                    onChange={(e) => setTeks(e.target.value)}
                    required
                  />
                  <button className="btn btn-success ml-3">Add</button>
                </InputGroup>
              </Form>
            </div>
          </div>
          <br />
        </div>
      </div>
      <div>
        <div className="flex flex-wrap md:gap-8 mx-9">
          {todo.map((todos) => (
            <div
              key={todos.id}
              className="border-green-700 border-2 rounded mt-2 p-2"
            >
              <div className="flex ">
                <p>{todos.selesai}</p>
                <p className="ml-auto">{todos.tanggalDibuat}</p>
              </div><hr />
              <div className="flex">
                <div className="">
                  <h2 className="text-2xl">{todos.taks}</h2>
                </div>
                <button
                  className="button-complete task-button ml-auto"
                  onClick={() => validasi (todos.id)}
                >
                  <i className="fas fa-check-circle"></i>
                  <div></div>
                </button>
              </div>
              <div className="mt-2">
                <a href={"/ediT/" + todos.id}>
                  <Button
                    variant="outline-primary"
                    className="mx-2 w-32 md:w-40"
                  >
                    <i className="fas fa-edit"></i>
                  </Button>
                </a>{" "}
                <Button
                  variant="outline-danger"
                  className="mx-2 w-32 md:w-40 lg:mx-8"
                  onClick={() => deleteUser(todos.id)}
                >
                  <i className="fas fa-trash"></i>
                </Button>
              </div>
            </div>
          ))}
        </div>
      </div>{" "}
    </div>
  );
}
