import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function () {
  const [show, setShow] = useState(false);
  const [presen, setPresensi] = useState([]);
  const [status, setStatus] = useState("");
  const [totalPage, setTotalPage] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const addMasuk = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://localhost:3009/presensi/masuk`, {
        role: "MASUK",
        status: status,
        userId: localStorage.getItem("id"),
      });
      sessionStorage.setItem("masuk", "m");
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Menambahkan Data!!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const pulang = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://localhost:3009/presensi/pulang`, {
        role: "PULANG",
        status: status,
        userId: localStorage.getItem("id"),
      });
      sessionStorage.setItem("pulang", "P");
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Menambahkan Data!!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:3009/presensi/all-presensi?userId=${localStorage.getItem(
          "id"
        )}&page=${page}`
      )
      .then((response) => {
        const pages = [];
        for (let i = 0; i < response.data.data.totalPages; i++) {
          pages.push(i);
        }
        setTotalPage(pages);
        setPresensi(
          response.data.data.content.map((data) => ({
            ...data,
          }))
        );
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const deleteT = async (id) => {
    await Swal.fire({
      title: "Yakin ingin menghapus data ini?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3009/presensi/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
    getAll(0);
  };

  return (
    <div>
      <div className="text-center mt-5">
        <h1 className="font-medium text-4xl text-green-700">
          <b>Absent Table </b> <i class="fas fa-clipboard-list"></i>
        </h1>
      </div>
      <div className="p-2 mt-4 flex gap-5 justify-between items-center">
        <button className="btn btn-outline-success" onClick={addMasuk}>
          <b>Absen Masuk</b>
          {/* <div>
          {sessionStorage.getItem("masuk") === null ? (
            <>Belom</>
          ) : (
            <>Sudah</>
          )}
          </div> */}
        </button>
        <div className="nav-item ml-2">
          <button className="btn btn-outline-success">
             <a onClick={pulang}>
            <b>
            Absen Pulang
            </b>
          </a>
          {/* <div>
          {sessionStorage.getItem("pulang") === null ? (
            <>Belom</>
          ) : (
            <>Sudah</>
          )}
          </div> */}
          </button>
        </div>
      </div>
      <div>
        <div className="flex flex-col">
          <div className="overflow-x-auto w-full">
            <div className="py-2 inline-block min-w-full">
              <div className="overflow-hidden">
                <table className="table shadow">
                  <thead className="border-b">
                    <tr
                      className=""
                    >
                      <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        No.
                      </th>
                      <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        Status
                      </th>
                      <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        Jam
                      </th>
                      {/* <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        ..
                      </th> */}
                      {/* <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        Status
                      </th> */}
                      <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        Tanggal
                      </th>
                      <th className="text-xl font-bold text-green-900 px-6 py-4 text-center">
                        Aksi
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {presen.map((daftar, index) => (
                      <tr
                        key={daftar.id}
                        style={{ color: "black", textAlign: "center" }}
                      >
                        <td>{index + 1}</td>

                        <td>{daftar.role}</td>
                        <td>{daftar.masuk}</td>
                        {/* <td>{daftar.status}</td> */}
                        <td>{daftar.tanggal}</td>
                        {localStorage.getItem("id") !== null ? (
                          <td>
                            <Button
                              variant="outline-danger"
                              className="mx-1"
                              onClick={() => deleteT(daftar.id)}
                            >
                              Delete
                            </Button>
                          </td>
                        ) : (
                          <></>
                        )}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              <ol class="flex justify-center gap-1 text-xs font-medium">
                <li>
                  <a
                    href="#"
                    class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                  >
                    <span class="sr-only">Prev Page</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-3 w-3"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                        clip-rule="evenodd"
                      />
                    </svg>
                  </a>
                </li>
                {/* pagination */}
                {totalPage.map((e, i) => (
                  <li>
                    <span
                      onClick={() => {
                        getAll(i);
                      }}
                      key={i}
                      class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
                    >
                      {i + 1}
                    </span>
                  </li>
                ))}
                <li>
                  <a
                    href="#"
                    class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                  >
                    <span class="sr-only">Next Page</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-3 w-3"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clip-rule="evenodd"
                      />
                    </svg>
                  </a>
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      {/* <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-green-700" closeButton>
          <Modal.Title>Attendance</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addMasuk}>
            <div className="mb-3">
              <Form.Label>
                <strong>Status</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Status"
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <a className="btn btn-danger" href="/absen">
              Close
            </a>
            ||
            <button className="btn btn-primary" onClick={handleClose}>
              Save
            </button>
          </Form>
        </Modal.Body>
      </Modal> */}
    </div>
  );
}
