import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function () {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telepon, setTelepon] = useState("");
  const [profile, setProfile] = useState("");
  const [role, setRole] = useState("USER");

  const register = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:3009/login1/sign-up", {
        email: email,
        password: password,
        nama: nama,
        alamat: alamat,
        telepon: telepon,
        profile: profile,
        role: role,
      });
      Swal.fire({
        icon: "success",
        title: "Register berhasil!!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
        history.push("/login");
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const history = useHistory();

  return (
    <div className="block md:flex">
      <div className="mt-8 p-5">
      <div className="text-green-700 font-semibold">
            <h1 style={{ textAlign: "center", fontSize: 40 }}>Formulir Daftar</h1>
            {/* <h4 >
            <a className="buat" href="/register"><i class="fas fa-user-edit"></i></a> 
          </h4> */}
          </div>
          {/* <hr style={{ height: 3, backgroundColor: "skyblue" }} /> <br /> */}
        <img className="w-1/2 ml-20 md:w-2/3 mt-5" src="https://cdn-icons-png.flaticon.com/512/166/166260.png" alt="" />
      </div>
      <div
        style={{ padding: 15, borderRadius: 10 }}
        className="container mt-10 w-full md:w-1/3"
      >
        <form
          onSubmit={register}
          method="POST"
          class="backdrop-opacity-20 backdrop-invert bg-white/0 shadow-md rounded px-8 pt-6 pb-8 mb-4"
        >
          
          <div class="mb-4 mt-2">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Email
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div class="mb-3">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Password
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              placeholder="******************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {/* <p class="text-red-700 text-xs italic">Please choose a password.</p> */}
          </div>
          <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Nama
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="nama"
              placeholder="Nama"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
            />
          </div>
          <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Alamat
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="alamat"
              placeholder="Alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
            />
          </div>
          <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              No Telpon
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="telepon"
              placeholder="0"
              value={telepon}
              onChange={(e) => setTelepon(e.target.value)}
            />
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
             Mendaftar
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-green-600 hover:text-green-800"
              href="/login"
            >
              Sudah memiliki akun? Gabung
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}
