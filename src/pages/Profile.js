import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import Footer from "../component/footer";

export default function () {
  const [profil, setProfil] = useState({
    profile: null,
    nama: "",
    alamat: "",
    telepon: "",
    password: "",
  });

  const [show, setShow] = useState(false);
  const [profile, setProfile] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telepon, setTelepon] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    axios
      .get("http://localhost:3009/login1/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setProfile(profil.profile);
        setNama(profil.nama);
        setEmail(profil.email);
        setAlamat(profil.alamat);
        setTelepon(profil.telepon);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Lurr!!! " + error);
      });
  }, []);

  const updateProfil = async (e) => {
    e.persist();
    e.preventDefault();

    const fromData = new FormData();
    fromData.append("file", profile);
    fromData.append("nama", nama);
    fromData.append("alamat", alamat);
    fromData.append("telepon", telepon);
    fromData.append("password", password);

    try {
      await axios.put(
        `http://localhost:3009/login1/${localStorage.getItem("id")}`,
        fromData
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit profile",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {}, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async () => {
    await axios
      .get("http://localhost:3009/login1/" + localStorage.getItem("id"))
      .then((res) => {
        setProfil(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };
  useEffect(() => {
    getAll(0);
  }, []);

  return (
    <div>
      <div className="p-6 ">
        <div className="hits">
          <div className="cil">
            <div
              className="md:w-full shadow-lg"
              style={{  backgroundColor: "", borderRadius: 10 }}
            >
              <div
                className="card backdrop-saturate-125 bg-white/30 p-5"
              >
                <div className="flex">
                  <div className="">
                    <h5 className="h5 text-green-800 text-2xl">Profil Saya</h5>
                    <p className=""> 
                      Kelola informasi profil Anda untuk mengontrol, melindungi
                      dan mengamankan akun
                    </p>
                  </div>
                  <div className="ml-auto">
              <a className="btn btn-success" href="/">
                <i class="fas fa-hand-point-left"></i>{" "}
              </a>
            </div>
                </div>
                <div></div>
                <hr className="h-2 bg-black gap-8" />
                <div
                  className="block md:flex 
                "
                  style={{padding: 20 }}
                >
                  <div>
                    <img
                      className="rounded-full md:rounded-2xl w-36 h-36 md:w-4/6 md:h-4/5 hover:translate-x-24"
                      src={
                        !profil.profile
                          ? "https://i.pinimg.com/474x/98/7a/df/987adf097a98ab1b32a953e2c33bba09.jpg"
                          : profil.profile
                      }
                      alt=""
                      // width={250}
                      // height={200}
                      type="file"
                    />
                    {/* <p className="text-center">{profil.role}</p> */}
                  </div>
                  <div className="text md:ml-96 p-2 mt-1">
                    <div className="flex gap-3 p-1 text-xl  border-green-600 roundedtext-green-600">
                    {/* <p>Nama :</p> */}
                    <h5 class="card-title text-xl"> { profil.nama}</h5>
                    </div><hr />
                    <h5 class="card-text text-2xl">
                    <i class="fa-solid fa-envelope"></i> {profil.email}
                    </h5>
                    <br />
                    <h5 class="card-text text-xl">
                      <i class="fas fa-map-marker-alt"></i> {profil.alamat}
                    </h5>
                    <br />
                    <h5 class="card-text text-xl">
                      <i class="fas fa-phone-volume"></i> {profil.telepon}
                    </h5>
                    {/* <h6 class="card-text">Password : {profil.password}</h6>*/}
                  </div>
                </div>
                <div className="prIn">
                  <Button
                    variant="outline-success"
                    onClick={handleShow}
                    type="button"
                    className="mx1 w-full"
                  >
                    Edit
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div >
        <Modal show={show} onHide={handleClose}>
          <Modal.Header className="bg-green-700" closeButton>
            <Modal.Title>Edit Profile</Modal.Title>
          </Modal.Header>
          <Modal.Body className="backdrop-opacity-20 backdrop-invert bg-white/0">
            <Form onSubmit={updateProfil}>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Foto Profile
                </label>
                <input
                  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  onChange={(e) => setProfile(e.target.files[0])}
                  type="file"
                  placeholder="Profil"
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Nama
                </label>
                <input
                  class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="nama"
                  placeholder="Nama"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Alamat
                </label>
                <input
                  class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="alamat"
                  placeholder="Alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />
              </div>
              <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  No Telpon
                </label>
                <input
                  class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  type="telepon"
                  placeholder="0"
                  value={telepon}
                  onChange={(e) => setTelepon(e.target.value)}
                />
              </div>
              <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2">
                  Password
                </label>
                <input
                  class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  type="password"
                  placeholder="******************"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <button className="btn btn-danger" onClick={handleClose}>
                Close
              </button>
              ||
              <button className="btn btn-primary" onClick={handleClose}>
                Save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
      {/* <Footer className="mt-1/2" /> */}
    </div>
  );
}
